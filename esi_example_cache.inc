<?php

/**
 * Implementation of a cache class that extends the Drupal memcache cache class
 * to add in ESI urls.
 *
 * Currently this class assumes that all items to be stored in this bin are
 * coming from render arrays.
 *
 * In addition, this class must assume that no Drupal modules are loaded if
 * $return_full_content is TRUE.
 */
class EsiExampleCacheDrupal extends MemCacheDrupal {
  /**
   * Whether this cache object should read full data from memcache or just
   * return ESI tags. Defaults to FALSE.
   */
  public $return_full_content;

  /**
   * Implements DrupalCacheInterface::__construct().
   *
   * Constructor with an added parameter that identifies if the cache object
   * in question should return the real $data['#markup'] or the proper ESI tag
   * to reduce overhead.
   */
  function __construct($bin, $return_full_content = FALSE) {
    $this->return_full_content = $return_full_content;

    parent::__construct($bin);
  }

  /**
   * Implements DrupalCacheInterface::get().
   */
  function get($cid) {
    if ($this->return_full_content) {
      return parent::get($cid);
    }
    else {
      // Drupal has been bootstrapped, so go nuts.
      if ($cache = parent::get($cid)) {
        // Cache hit! Unfortunately, we have fetched the full HTML but we will
        // replace the output with the ESI tag.

        // @TODO: This seems wasteful. In the future, store $data['#attached']
        // in a separate key so that we can avoid sending the HTML over the
        // wire only to throw it away?

        // At least try to save a little memory.
        unset($cache->data['#markup']);

        $cache->data['#markup'] = $this->renderEsiTag($cid, TRUE);

        return $cache;
      }
      else {
        // Cache miss.
        return FALSE;
      }
    }
  }

  /**
   * Implements DrupalCacheInterface::set().
   */
  function set($cid, $data, $expire = CACHE_PERMANENT, array $headers = NULL) {
    // Add some HTTP caching headers based on the cache item's expiration.
    $data['#headers'] = array();
    if ($expire == CACHE_PERMANENT) {
      $data['#headers'] = array('Cache-Control: public, max-age=' . variable_get('esi_example_default_ttl', 3600));
    }
    else {
      $data['#headers'] = array('Cache-Control: public, max-age=' . $expire);
    }

    parent::set($cid, $data, $expire);

    $data['#markup'] = $this->renderEsiTag($cid, FALSE);
  }

  /**
   * Given a cid, render an ESI tag. Note whether it was a cache hit or cache
   * miss.
   *
   * @TODO: Since this should always be called when Drupal is bootstrapped,
   * consider using a theme() function.
   */
  function renderEsiTag($cid, $cache_hit = FALSE) {
    // With the item properly set into memcache, alter the markup returned to
    // be an ESI include tag.
    $markup = '<esi:include src="/esi.php?cid=' . base64_encode($cid) .'"></esi:include>';

    if ($cache_hit) {
      $markup .= '<!-- ESI cache hit -->';
    }
    else {
      $markup .= '<!-- ESI cache miss -->';
    }

    return $markup;
  }

  // @TODO: Make sure that drupal_render_cache_get() uses a memcached 'append'
  // command with a null value to test for key existence to avoid reading the
  // entire rendered item over the wire just to print an esi:include tag.
}
