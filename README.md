# Memcache-based ESI for Drupal 7

## Introduction

This module is a simple prototype of how ESI can be accomplished using the new Drupal 7 Render API.

## Limitations

1. Currently, this module will only work properly with render arrays that are cached DRUPAL\_CACHE\_PER\_PAGE or DRUPAL\_CACHE\_GLOBAL. In order to do DRUPAL\_CACHE\_PER\_ROLE or DRUPAL\_CACHE\_PER\_USER, that information will need to be accessible without a bootstrap from the esi.php script.
2. Currently, drupal\_render\_cache\_get() will pull the full item from memcached and render it without an ESI tag.

