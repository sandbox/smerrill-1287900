<?php

/**
 * A simple, non-Drupal PHP script to pull a value from memcache.
 *
 * This script must be symlinked into the root level of the docroot so that it
 * can read settings.php for memcache information.
 */

// For now, we need bootstrap.inc so that variable_get() exists.
// This should also let us bootstrap later if needed.
require_once './includes/bootstrap.inc';

// Get started.
// @TODO: Make dynamic in a simple way.
require_once './sites/default/settings.php';

$memcache = new EsiExampleCacheDrupal('cache_esi_example', TRUE);
if ($cached_item = $memcache->get(base64_decode($_GET['cid']))) { 
  $data = $cached_item->data;
  if ($data['#headers']) {
    foreach ($data['#headers'] as $header) {
      header($header);
    }
  }
  print $data['#markup'];

  exit;
}
else {
  // @TODO: Implement per-URL locking so that we can get as many items
  // regenerated as possible in a single bound w/o contention.

  // @TODO: Add a second URL parameter that should allow bootstrapping Drupal
  // to hit a single URL and produce the item and set its markup back into the
  // cache in case things get out of sync.

  die('You are eaten by a grue.');
}


